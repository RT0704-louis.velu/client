import requests
import json
import time
#config:
#server
SERVER_IP="10.0.2.15"
SERVER_PORT="5000"
#login
USER_ID = "1"
PASSWORD = "1"

try:
	#job1 graphviz
	data_job1 = {
		"id": USER_ID,
		"password": PASSWORD,
		"filename": "graphviz.png",
		"type": "graphviz",
		"format": "png",
		"args" : ""
	}
	file = {"file": open("./test_file/graphviz.dot","rb")}
	resp = requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/submit", files=file, data={"data": json.dumps(data_job1)})
	if resp.status_code != 200:
		print("Server IP: "+SERVER_IP+" Server Port: "+SERVER_PORT+" HTTP satus code: "+ str(resp.status_code))
		print("reponse: "+ str(resp))
	else:
		data_job1 = json.loads(resp.content.decode('utf-8'))
		if "erreur" in data_job1:
			print("erreur: "+data_job1["erreur"])
		else:
			id_job1 = data_job1["job_id"]


	#job2 pandoc
	data_job2 = {
		"id": USER_ID,
		"password": PASSWORD,
		"filename": "pandoc.pdf",
		"type": "pandoc",
		"format": "pdf",
		"args" : ""
	}
	file = {"file": open("./test_file/pandoc.md","rb")}
	resp = requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/submit", files=file, data={"data": json.dumps(data_job2)})
	if resp.status_code != 200:
		print("Server IP: "+SERVER_IP+" Server Port: "+SERVER_PORT+" HTTP satus code: "+ str(resp.status_code))
		print("reponse: "+ str(resp))
	else:
		data_job2 = json.loads(resp.content.decode('utf-8'))
		if "erreur" in data_job2:
			print("erreur: "+data_job2["erreur"])
		else:
			id_job2 = data_job2["job_id"]


	#job3 image_magick
	data_job3 = {
		"id": USER_ID,
		"password": PASSWORD,
		"filename": "image_magick.png",
		"type": "image_magick",
		"format": "png",
		"args" : "-resize \"500x500\""
	}
	file = {"file": open("./test_file/image_magick.jpg","rb")}
	resp = requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/submit", files=file, data={"data": json.dumps(data_job3)})
	if resp.status_code != 200:
		print("Server IP: "+SERVER_IP+" Server Port: "+SERVER_PORT+" HTTP satus code: "+ str(resp.status_code))
		print("reponse: "+ str(resp))
	else:
		data_job3 = json.loads(resp.content.decode('utf-8'))
		if "erreur" in data_job3:
			print("erreur: "+data_job3["erreur"])
		else:
			id_job3 = data_job3["job_id"]

	#on récupère le résultat du job1
	data_job1 = {"id": data_job1["job_id"]}
	resp = requests.get("http://"+SERVER_IP+":"+SERVER_PORT+"/get-result", params={"data": json.dumps(data_job1)})
	if resp.status_code != 200:
		print("Server IP: "+SERVER_IP+" Server Port: "+SERVER_PORT+" HTTP satus code: "+ str(resp.status_code))
		print("reponse: "+ str(resp))
	else:
		while resp.headers["content-type"] == "text/html; charset=utf-8":
			if json.loads(resp.content.decode('utf-8'))["erreur"] != "result not available":
				print("Job1 erreur: "+json.loads(resp.content.decode('utf-8'))["erreur"])
				break

			time.sleep(1)
			resp = requests.get("http://"+SERVER_IP+":"+SERVER_PORT+"/get-result", params={"data": json.dumps(data_job1)})

		if resp.headers["content-type"] != "text/html; charset=utf-8":
			print(str(resp.headers))
			open("./result/"+resp.headers['Content-Disposition'].split("filename=")[1], 'wb').write(resp.content)
	print("JOB1 fini !")



	#on récupère le résultat du job2
	data_job2 = {"id": data_job2["job_id"]}
	resp = requests.get("http://"+SERVER_IP+":"+SERVER_PORT+"/get-result", params={"data": json.dumps(data_job2)})
	if resp.status_code != 200:
		print("Server IP: "+SERVER_IP+" Server Port: "+SERVER_PORT+" HTTP satus code: "+ str(resp.status_code))
		print("reponse: "+ str(resp))
	else:
		while resp.headers["content-type"] == "text/html; charset=utf-8":
			if json.loads(resp.content.decode('utf-8'))["erreur"] != "result not available":
				print("Job2 erreur: "+json.loads(resp.content.decode('utf-8'))["erreur"])
				break

			time.sleep(1)
			resp = requests.get("http://"+SERVER_IP+":"+SERVER_PORT+"/get-result", params={"data": json.dumps(data_job2)})

		if resp.headers["content-type"] != "text/html; charset=utf-8":
			open("./result/"+resp.headers['Content-Disposition'].split("filename=")[1], 'wb').write(resp.content)

	print("JOB2 fini !")



	#on récupère le résultat du job3
	data_job3 = {"id": data_job3["job_id"]}
	resp = requests.get("http://"+SERVER_IP+":"+SERVER_PORT+"/get-result", params={"data": json.dumps(data_job3)})
	if resp.status_code != 200:
		print("Server IP: "+SERVER_IP+" Server Port: "+SERVER_PORT+" HTTP satus code: "+ str(resp.status_code))
		print("reponse: "+ str(resp))
	else:
		while resp.headers["content-type"] == "text/html; charset=utf-8":
			if json.loads(resp.content.decode('utf-8'))["erreur"] != "result not available":
				print("Job3 erreur: "+json.loads(resp.content.decode('utf-8'))["erreur"])
				break

			time.sleep(1)
			resp = requests.get("http://"+SERVER_IP+":"+SERVER_PORT+"/get-result", params={"data": json.dumps(data_job3)})

		if resp.headers["content-type"] != "text/html; charset=utf-8":
			open("./result/"+resp.headers['Content-Disposition'].split("filename=")[1], 'wb').write(resp.content)

	print("JOB3 fini !")

except:
	print("server introuvable")
